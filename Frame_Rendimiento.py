from tkinter import *
from PIL import ImageTk, Image
from tkinter import ttk, font


def Refresher():
    archivo=open(r"C:\Users\rgonzalezn\Desktop\Frame_1\Datos_frame\Frame.txt","r")
    n=archivo.readlines()
    nombre.set(n[0])
    entry.after(100, Refresher)


if __name__ == '__main__':

    raiz = Tk()
    nombre = StringVar()
    Grua = StringVar()
    Mov_Hora = StringVar()
    Last_mov = StringVar()
    Espera_carga = StringVar()

    raiz.geometry('1300x800')  # ancho x alto
    fuente = font.Font(weight='normal', size=15)

    raiz.configure(bg='light blue')
    img = Image.open('sti.jpg')

    raiz.title('San Antonio Terminal Internacional Gruas')
    leftFrame = ttk.Frame(raiz)
    leftFrame.grid(row=1, column=0, sticky='w')
    rightFrame = ttk.Frame(raiz)
    rightFrame.grid(row=1, column=2)

    tkimage = ImageTk.PhotoImage(img)

    label = Label(rightFrame, image=tkimage, width=86, height=105)

    archivo = open(r"C:\Users\rgonzalezn\Desktop\Frame_1\Datos_frame\Frame.txt", "r")
    n = archivo.readlines()
    nombre.set(n[0])
    Grua.set("STS1")
    Mov_Hora.set("10")
    Last_mov.set("10")
    Espera_carga.set("10")

    entry = ttk.Entry(leftFrame, state="readonly", textvariable=nombre, width=50, font=fuente)
    entry1 = ttk.Entry(leftFrame, state="readonly", textvariable=Grua, font=fuente)
    entry2 = ttk.Entry(leftFrame, state="readonly", textvariable=Mov_Hora, font=fuente)
    entry3 = ttk.Entry(leftFrame, state="readonly", textvariable=Last_mov, font=fuente)
    entry4 = ttk.Entry(leftFrame, state="readonly", textvariable=Espera_carga, font=fuente)
    entry.after(100, Refresher)

    etiqueta = Label(leftFrame, text='Operador:', background="light blue", font=fuente)
    etiqueta1 = Label(leftFrame, text='Grúa:', background="light blue", font=fuente)
    etiqueta2 = Label(leftFrame, text='Moviminetos/Hora:', background="light blue", font=fuente)
    etiqueta3 = Label(leftFrame, text='Último Movimiento[Seg]:', background="light blue", font=fuente)
    etiqueta4 = Label(leftFrame, text='Espera De Carga [Min]:', background="light blue", font=fuente)

    etiqueta.grid(row=1, column=0, sticky='we')
    etiqueta1.grid(row=2, column=0, sticky='we')
    etiqueta2.grid(row=3, column=0, sticky='we')
    etiqueta3.grid(row=4, column=0, sticky='we')
    etiqueta4.grid(row=5, column=0, sticky='we')

    entry.grid(row=1, column=1, columnspan=2, sticky='we')
    entry1.grid(row=2, column=1, columnspan=2, sticky='we')
    entry2.grid(row=3, column=1, columnspan=2, sticky='we')
    entry3.grid(row=4, column=1, columnspan=2, sticky='we')
    entry4.grid(row=5, column=1, columnspan=2, sticky='we')

    label.grid(row=0, column=2, sticky='e')
    raiz.columnconfigure(0, weight=1)
    raiz.columnconfigure(1, weight=1)
    raiz.mainloop()



