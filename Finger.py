import os
import os.path as path
import adafruit_fingerprint
import serial


#########################################################################
#  Programa de Rendimiento de Grúas STS Realizado Por                   #
#   Sebastian Barbosa Martorell                                         #
#   Roberto Gonzalez Navarro                                            #
#        02/01/2020                                                     #
#                                                                       #
#########################################################################


#Detection de Puerto COM

y = 0
while True:
    y += 1
    puerto = "COM" + str(y)
    try:
        uart = serial.Serial(puerto, baudrate=57600, timeout=1)
    except serial.serialutil.SerialException:
        print("Puerto no detectado")
    else:
        print("Huellero encontrado en el puerto {}".format(puerto))
        break

finger = adafruit_fingerprint.Adafruit_Fingerprint(uart)


########################################################################################

# Función Revisión si el numero ingresado existe o se encuentra ocupado por otra persona

def Check_txt(numb):
    archivo=open(r"C:\Users\rgonzalezn\Desktop\Frame_1\Oper.txt","r")
    for linea in archivo.read():
        if linea==str(numb):
            print ("ese numero ya esta ocupado ingrese otro valor")
            return True
    return False

#######################################################################################

#Función de Revision de huella que detectada

def get_fingerprint():
    print("Coloque el Dedo...")
    while finger.get_image() != adafruit_fingerprint.OK:
        pass
    print("Templating...")
    if finger.image_2_tz(1) != adafruit_fingerprint.OK:
        return False
    print("Buscando Dedo...")
    if finger.finger_fast_search() != adafruit_fingerprint.OK:
        return False
    return True

def get_fingerprint_detail():
    print("Getting image...", end="", flush=True)
    i = finger.get_image()
    if i == adafruit_fingerprint.OK:
        print("Image taken")
    else:
        if i == adafruit_fingerprint.NOFINGER:
            print("No finger detected")
        elif i == adafruit_fingerprint.IMAGEFAIL:
            print("Imaging error")
        else:
            print("Other error")
        return False

    print("Templating...", end="", flush=True)
    i = finger.image_2_tz(1)
    if i == adafruit_fingerprint.OK:
        print("Templated")
    else:
        if i == adafruit_fingerprint.IMAGEMESS:
            print("Image too messy")
        elif i == adafruit_fingerprint.FEATUREFAIL:
            print("Could not identify features")
        elif i == adafruit_fingerprint.INVALIDIMAGE:
            print("Image invalid")
        else:
            print("Other error")
        return False

    print("Searching...", end="", flush=True)
    i = finger.finger_fast_search()
    if i == adafruit_fingerprint.OK:
        print("Found fingerprint!")
        return True
    else:
        if i == adafruit_fingerprint.NOTFOUND:
            print("No match found")
        else:
            print("Other error")
        return False

################################################################################################

#Función para ingresar ubicación  y nombre al archivo que almacenara los datos de los usuarios

def get_num():
    """Use input() to get a valid number from 1 to 127. Retry till success!"""
    i=input("Ingrese ubicación :")
    return i
def get_name():
    i=input("Ingrese Su Nombre Completo: ")
    return i

#################################################################################################

#Función que saca solo el nombre en un archivo y guardarlo en otro para despues ser rescatado por el programa de TKinter

def Name(id):
    archivo=open(r"C:\Users\rgonzalezn\Desktop\Frame_1\Oper.txt","r")
    print(id)
    for x in archivo:
        if str(id) == x.split("\t")[0]:
            nombre=(x.split("\t")[1].strip("\n"))
            if path.exists(r"C:\Users\rgonzalezn\Desktop\Frame_1\Datos_frame\Frame.txt"):
                if os.stat(r"C:\Users\rgonzalezn\Desktop\Frame_1\Datos_frame\Frame.txt").st_size == 0:
                    archivo=open(r"C:\Users\rgonzalezn\Desktop\Frame_1\Datos_frame\Frame.txt","w")
                    archivo.write("ANONYMOUS")
                    archivo.close()
                else:
                    archivo=open(r"C:\Users\rgonzalezn\Desktop\Frame_1\Datos_frame\Frame.txt","w")
                    archivo.write(nombre)
                    archivo.close()
            else:
                archivo=open(r"C:\Users\rgonzalezn\Desktop\Frame_1\Datos_frame\Frame.txt","w")
                archivo.write("ANONYMOUS")
                archivo.close()

##################################################################################################

#loop del programa que esta constantemente mirando el huellero

while True:
    if get_fingerprint():
        print("Detectado #", finger.finger_id, "coincidencia", finger.confidence)
        if finger.confidence >= 70:
            Name(finger.finger_id)

